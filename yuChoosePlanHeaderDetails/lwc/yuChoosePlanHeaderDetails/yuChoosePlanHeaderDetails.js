import { LightningElement, api, track } from 'lwc';
import { OmniscriptBaseMixin } from 'vlocity_cmt/omniscriptBaseMixin';

export default class YuChoosePlanHeaderDetails extends OmniscriptBaseMixin(LightningElement){

    _meterList;
    _allowance;

    
    @track detailsToDisplay = [];
    @track isInValid = true;
    @track isGas = 'ELECTRICITY';
    @track isModalOpen = false;
    @track msgBody1;
    @track msgBody2;
    @track headerMsg;
    @api consumption;
    @api meterNumber;
    @api commodityType;
    @api allowance;

   
    @api 
    get meterList()
    {
        return this._meterList;
    }
    set meterList(json) {
     
            this._meterList = json;
            console.log('this.mter',JSON.parse(JSON.stringify(this.meterList)));
            this.detailsToDisplay = JSON.parse(JSON.stringify(this.meterList));
        
    }

    connectedCallback() {
        this.isInValid = true;
        if(this.commodityType == "Electricity"){
            this.isGas = 'ELECTRICITY';
        }else{
            this.isGas = 'GAS';
        }

    }

    handleChange(event){
        if (event.target.name === "duration"){
            this.duration = event.target.value;
            console.log("value for duration ",this.duration);
        }    
        if (event.target.name === "usage") {
            if(this.duration == "perMonth"){
                    this.usage = event.target.value * 12;
                    console.log("value for usage 222222222222 ",this.usage);  
                }
            else{
                    this.usage = event.target.value;
                    console.log("value for usage33333 ",this.usage);
                }
            }
            this.isInValid = false;
            console.log("values",this.usage ,this.consumption,this.allowance );
            let upperLimit =parseInt( this.consumption) + parseInt((this.consumption * (this.allowance / 100)));
            console.log("Upper limit=====", upperLimit);
            let lowerLimit = this.consumption - (this.consumption * (this.allowance /100));
            console.log("Lower limit=====", lowerLimit);
            if (this.usage > upperLimit 
                || this.usage < lowerLimit) {
                this.isError = true;
                this.isInValid = true;
                this.isModalOpen = true;
                this.msgBody1 = "Your Consumption seems to have moved a lot from the amount the industry \n provided for your meter points";
                this.msgBody2 = "Please call us to complete your business energy quote";
                this.headerMsg = "CONSUMPTION ERROR"

        
            } else {
                this.isError = false;
                this.isInValid = false;
                this.isModalOpen = false;
            }
        }
        
    

    handleProceed(event) {
        if (event.target.name == "usage") {
            this.usage =  event.target.value;
            console.log("value of usage*****", event.target.value );
        }
            
        console.log("value of usage&&&&&&", this.usage );
        this.usage = this.usage ? Number(this.usage) : null;
        if (this.commodityType == 'Electricity') {
            this.omniApplyCallResp({ CustomerEAC: this.usage });
            console.log("value of usage88888888", this.usage );
        }else{
            this.omniApplyCallResp({ CustomerAQ: this.usage });
        }
    
    }
    closeModal() {
        this.isModalOpen = false;
    }
}