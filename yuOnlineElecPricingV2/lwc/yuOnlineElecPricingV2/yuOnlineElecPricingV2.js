/* eslint-disable no-irregular-whitespace */
/* Author        : Cloobees
* Date           : 10/12/2021     
* Description    : This component is used to display the plan details in the Online Journey OS.  
* Modification Log:
--------------------------------------------------------------------------------------------
* Developer Name                         Date            Description
* Cloobees                              10/12/2021      Initial version
*/
import { LightningElement, api, track } from 'lwc';
import { OmniscriptBaseMixin } from "vlocity_cmt/omniscriptBaseMixin";
import { getNamespaceDotNotation } from "vlocity_cmt/omniscriptInternalUtils";
import { loadScript } from "lightning/platformResourceLoader";
import { OmniscriptActionCommonUtil } from 'vlocity_cmt/omniscriptActionUtils';

export default class yuOnlineElecPricingV2 extends OmniscriptBaseMixin(LightningElement) {

    //Reactive variables
    _digitalCommerce = null;
    _dcSdkPath = "/resource/vlocity_cmt__vlocitydcsdk/latest/digitalcommerce/digitalcommerce.sdk.js";
    _dcSdkApiUrl = "/services/apexrest";
    _sfdcUtilsApexCallConfig;
    _catalogCode;
    _productCode;
    _jsonResult;
    _multicommodity;
    _ns = getNamespaceDotNotation();
    _actionUtilClass;
    _commodity;
    _inputData;
    availableSfdcUrl;
    availableSfdcToken;
    mapOfferDetails = new Map();
    listDurationMonths = ["12","24","36"];
    greenEnergyGas = ["STANDARD","CARBON NEUTRAL"];
    greenEnergyElec = ["STANDARD","GREEN"];
    monthlyCost;
    annualCost;
    contractCost;
    standardCharge;
    standingCharge;
    dayCharge;
    nightCharge;
    eveningWeekendCharge;
    offPeakCharge;
    day1Charge;
    day2Charge;
    day3Charge;
    day4Charge;
    @track isLoaded = false;
    @track isFailed = false;
    @track isPriceLoaded = false;
    @track isModalOpen = false;
    @track planSelected = false;
    @track isEmailQuote = false;
    @track planDetails;
    usage;
    rateStructure;
    isMultiCommodity = false;
    @track cssStyleClass;
    isDesktopSize;
    headerMsg;
    msgBody1;
    msgBody2;
    btnLabel;
    iconName;
    month;
    year;
    startDt;
    chargeHeader = {};
    @track prices = [];
    planDetailsArr = [];

    @api
    get catalogCode() {
        return this._catalogCode;
    }
    set catalogCode(value) {
        if (value)
            this._catalogCode = value;
    }

    @api
    get inputData() {
        return this._inputData;
    }
    set inputData(value) {
        if (value)
            this._inputData = value;
    }

    @api
    get productCode() {
        return this._productCode;
    }
    set productCode(value) {
        if (value)
            this._productCode = value;
    }

    @api
    get commodity() {
        return this._commodity;
    }
    set commodity(value) {
        if (value)
            this._commodity = value;
    }
    @api
    get multiCommodity() {
        return this._multiCommodity;
    }
    set multiCommodity(value) {
        if (value)
            this._multiCommodity = value;
    }
    multiCommodity
    @api
    get sfdcUtilsApexCallConfig() {
        return this._sfdcUtilsApexCallConfig;
    }
    set sfdcUtilsApexCallConfig(value) {
        if (value)
            this._sfdcUtilsApexCallConfig = value;
    }

    @api
    get isDesktopSize() {
        return window.innerWidth > 600;
    }

    /*
    Method Name : getSalesforceInfo
    Description : used to retrive the sdk initialization helpers
    Paramerter  : sfdcUtilsApexCallConfig
    */
    getSalesforceInfo(sfdcUtilsApexCallConfig) {
        if (this.availableSfdcUrl) {
            return new Promise((resolve, reject) => {
                resolve([this.availableSfdcUrl, this.availableSfdcToken]);
            });
        } else {
            return new Promise((resolve, reject) => {
                const params = {
                    input: '{}',
                    sClassName: 'VlocityUtils',
                    sMethodName: 'salesforceInfo',
                    options: '{}'
                };

                this.omniRemoteCall(params, false)
                    .then(response => {
                        this.availableSfdcToken = response.result.salesforceToken;
                        this.availableSfdcUrl = response.result.salesforceUrl;
                        resolve([response.result.salesforceUrl, response.result.salesforceToken]);
                    })
                    .catch(error => {
                        window.console.log(error, 'getSalesforceInfo error');
                        this.omniUpdateDataJson({ "APIMessage": error });
                    })
            });
        }
    }

    /*
    Method Name : getDigitalCommerceSDKInstance
    Description : gets the SDK instance for the transaction
    Paramerter  : sfdcUrl, sfdcToken, isLoggedInUser
    */
    getDigitalCommerceSDKInstance(sfdcUrl, sfdcToken, isLoggedInUser) {
        return new Promise((resolve, reject) => {
            loadScript(this, this._dcSdkPath)
                .then(() => {
                    // Instantiate DigitalCommerceConfig
                    if (this.externalUrl != null) {
                        sfdcUrl = this.externalUrl; // override external
                    }
                    const digitalCommerceConfig = (isLoggedInUser == true) ? window.VlocitySDK.digitalcommerce.createConfigForLoginUser(sfdcUrl, sfdcToken) : window.VlocitySDK.digitalcommerce.createConfigForAnonymousUser();
                    const digitalCommerce = window.VlocitySDK.digitalcommerce.getInstance(digitalCommerceConfig);
                    digitalCommerce.namespace = "vlocity_cmt";
                    digitalCommerce.apiURL = sfdcUrl + this._dcSdkApiUrl + '/' + digitalCommerce.namespace;
                    resolve(digitalCommerce);
                })
                .catch((e) => {
                    resolve(null);
                    console.log("DC SDK not loaded");
                    this.omniUpdateDataJson({ "APIMessage": e });

                });
        });
    }

    /*
    Method Name : invokeGetOfferDetails
    Description : invokes the get offer details for list of offers
    Paramerter  : NA
    */
    invokeGetOfferDetails() {
        if (this.mapOfferDetails && this.mapOfferDetails.get(this.productCode)) {
            return new Promise((resolve, reject) => {
                resolve(this.mapOfferDetails.get(this.productCode));
            });
        } else {
            return new Promise((resolve, reject) => {
                // Instantiate the input object for getOfferDetails to specify parameters
                const input = this._digitalCommerce.createGetOfferInput();
                input.apiNamespace = "cacheableapi";
                input.actionObj = {
                    client: {
                        records: [],
                        params: {}
                    },
                    remote: {
                        params: {}
                    },
                    rest: {
                        params: {},
                        method: "GET",
                        link: "/v3/catalogs/" + this._catalogCode + "/offers/" + this._productCode
                    }
                };

                // Invoke GetOfferDetails API via method getOfferDetails()
                this._digitalCommerce
                    .invokeAction(input)
                    .then(response => {
                        this.mapOfferDetails.set(this._productCode, response.result);
                        resolve(response.result);
                    })
                    .catch(error => {
                        console.log("vlocity get offerDetails rest call failed - " + error);;
                        this.omniUpdateDataJson({ "APIMessage": error });
                        this.isLoading = false;
                    });
            });
        }
    }

    /*
    Method Name : connectedCallback
    Description : This function will be called on load of the component to the DOM
    Paramerter  : NA
    */
    connectedCallback() {
        this.isLoaded = false;
        this.cssStyleClass = 'nds-grid nds-wrap nds-align_absolute-center';

        this._actionUtilClass = new OmniscriptActionCommonUtil();
        this.getSalesforceInfo(this._sfdcUtilsApexCallConfig)
            .then(([sfdcUrl, sfdcToken]) => {
                this.getDigitalCommerceSDKInstance(sfdcUrl, sfdcToken, sfdcToken !== null)
                    .then((res) => {
                        this._digitalCommerce = res;
                        this.invokeGetOfferDetails()
                            .then((jsonResult) => {
                                this.startDt = this.inputData.startDate;
                                this._jsonResult = this.injectUserDataIntoGetOfferDetails(jsonResult);
                                this.calculatePrices();
                            })
                            .catch((err) => {
                                console.log("invokeGetOfferDetails failed", err);
                            });
                    })
                    .catch((err) => {
                        console.log("DC SDK Instance creation failed", err);
                    });
            })
            .catch((err) => {
                console.log("getSalesforceInfo failed", err);
            });
    }

    /*
    Method Name : findAndReplace
    Description : This function is used to find and replace the value in the get Offer Details response json
    Paramerter  : object, key, value, replacekey, replacevalue
    */
    findAndReplace(object, key, value, replacekey, replacevalue) {
        for (var x in object) {
            if (object.hasOwnProperty(x)) {
                if (object[x] == value && x == key) {
                    object[replacekey] = replacevalue;
                    break;
                } else if (typeof object[x] == 'object') {
                    this.findAndReplace(object[x], key, value, replacekey, replacevalue);
                }
            }
        }
    }

    /*
    Method Name : injectUserDataIntoGetOfferDetails
    Description : updates the response of the GetOfferDetails with the values
    Paramerter  : jsonResult
    */
    injectUserDataIntoGetOfferDetails(jsonResult) {
        
        this.usage = this.inputData.usage;
        this.isGreen = this.inputData.greenEnergy ? true : false;
        this.smartMeter = this.inputData.smartMeter ? true : false;
        this.rateStructure = this.inputData.rateStructure;
        let greenEnergyAttribute;
        console.log("this.startDt======",this.startDt);
        this.findAndReplace(jsonResult, "code", "ATT_CONTRACT_START_DATE", "userValues", this.startDt);
        this.findAndReplace(jsonResult, "code", "ATT_STANDING_CHARGE_TYPE", "userValues", this.inputData.chargeType);
        this.findAndReplace(jsonResult, "code", "ATT_CREDIT_SCORE", "userValues", this.inputData.creditScore);
        this.findAndReplace(jsonResult, "code", "ATT_ANNUALCONSUMPTION", "userValues", this.usage);
        if (this._commodity == 'GAS') {
           // greenEnergyAttribute = this.inputData.greenEnergy ? 'CARBON NEUTRAL' : 'STANDARD';
            //this.findAndReplace(jsonResult, "code", "ATT_GREENENERGY_GAS", "userValues", greenEnergyAttribute);
            this.findAndReplace(jsonResult, "code", "ATT_LDZ", "userValues", this.inputData.ldz);
            this.findAndReplace(jsonResult, "code", "ATT_EXITZONE", "userValues", this.inputData.exitZone);
			this.findAndReplace(jsonResult, "code", "ATT_SMART_GAS", "userValues", this.inputData.smartPrice);

            //add products to the basket
            this.findAndReplace(jsonResult, "ProductCode", "PRD_STANDARD_CHARGE", "Quantity", 1);
            this.findAndReplace(jsonResult, "ProductCode", "PRD_STANDING_CHARGE", "Quantity", 1);
        } else {
            //greenEnergyAttribute = this.inputData.greenEnergy ? 'GREEN' : 'STANDARD';
            //this.findAndReplace(jsonResult, "code", "ATT_GREEN_ENERGY_ELEC", "userValues", greenEnergyAttribute);
            this.findAndReplace(jsonResult, "code", "ATT_DNOID", "userValues", this.inputData.dnoId);
            this.findAndReplace(jsonResult, "code", "ATT_RATE_STRUCTURE", "userValues", this.inputData.rateStructure.rateStructure);
            this.findAndReplace(jsonResult, "code", "ATT_PROFILE_CLASS", "userValues", this.inputData.profileClass);
            this.findAndReplace(jsonResult, "code", "ATT_SMART_ELEC", "userValues", this.inputData.smartPrice);
            //add products to the basket
            this.findAndReplace(jsonResult, "ProductCode", "PRD_STANDING_CHARGE", "Quantity", 1);
            if (this.inputData.rateStructure) {
                if (this.inputData.rateStructure.day) {
                    this.findAndReplace(jsonResult, "ProductCode", "PRD_DAY_CHARGE", "Quantity", 1);
                }
                if (this.inputData.rateStructure.night) {
                    this.findAndReplace(jsonResult, "ProductCode", "PRD_NIGHT_UNIT_CHARGE", "Quantity", 1);
                }
                if (this.inputData.rateStructure.standard) {
                    this.findAndReplace(jsonResult, "ProductCode", "PRD_STANDARD_CHARGE", "Quantity", 1);
                }
                if (this.inputData.rateStructure.offPeak) {
                    this.findAndReplace(jsonResult, "ProductCode", "PRD_OFF_PEAK_UNIT_CHARGE", "Quantity", 1);
                }
                if (this.inputData.rateStructure.eveningWeekend) {
                    this.findAndReplace(jsonResult, "ProductCode", "PRD_EVENING_WEEKEND_CHARGE", "Quantity", 1);
                }
                if (this.inputData.rateStructure.day1) {
                    this.findAndReplace(jsonResult, "ProductCode", "PRD_DAY_1_CHARGE", "Quantity", 1);
                }
                if (this.inputData.rateStructure.day2) {
                    this.findAndReplace(jsonResult, "ProductCode", "PRD_DAY_2_CHARGE", "Quantity", 1);
                }
                if (this.inputData.rateStructure.day3) {
                    this.findAndReplace(jsonResult, "ProductCode", "PRD_DAY_3_CHARGE", "Quantity", 1);
                }
                if (this.inputData.rateStructure.day4) {
                    this.findAndReplace(jsonResult, "ProductCode", "PRD_DAY_4_CHARGE", "Quantity", 1);
                }
                if (this.inputData.rateStructure.day5) {
                    this.findAndReplace(jsonResult, "ProductCode", "PRD_DAY_5_CHARGE", "Quantity", 1);
                }
            }
        }
        if (this.inputData.isMultiCommodity) {
            this.isMultiCommodity = true;
        } else {
            this.isMultiCommodity = false;
        }
        //this.isLoaded = true;
        this.loadCard = true;
        return jsonResult;
    }


    /*
    Method Name : handleOnFailure
    Description : this will be called in case of the failure 
    Paramerter  : event
    */
    handleOnFailure() {
        console.log('Inside handleOnFailure=====');
        this.headerMsg = 'WE NEED MORE INFO';
        this.msgBody1 = "Oh no, we can't proceed with this plan";
        this.msgBody2 = 'Please call our team to proceed with your energy quote';
        this.btnLabel = '0115 975 8258';
        this.iconName = "standard:call";
    }

    /*
    Method Name : handleClick
    Description : This function will be called on click
    Paramerter  : NA
    */
    handleClick() {

    }

    /*
    Method Name : closeModal
    Description : closes the modal pop up and navigates to previous steps
    Paramerter  : NA
    */
    closeModal() {
        this.isModalOpen = false;
        this.isFailed = false;
    }

    calculatePrices() {
        this.prices = [];
        this.prices.length = 0;
        if(this._commodity == 'GAS') {
            for(let greenGas of this.greenEnergyGas) {
                console.log('greenGas=====' + greenGas);
                for(let duration of this.listDurationMonths) {
                    this.invokeCreateBasket(greenGas,duration);
                }
            }
        } else {
            for(let greenElec of this.greenEnergyElec) {
                for(let duration of this.listDurationMonths) {
                    //this.invokeCreateBasket(this.greenEnergyElec[key],this.listDurationMonths[key1]);
                    this.invokeCreateBasket(greenElec,duration);
                }
            }
        }    
        
    }
    /*
    Method Name : handleOnSuccess
    Description : stops the loading spinner and displays the child cards with pricing
    Paramerter  : event
    */
    handleOnSuccess(event) {
        this.loadedChildCards.push(event.detail);
        if (this.loadedChildCards.length == 3 && this.loadedChildCards.indexOf('card1') >= 0 &&
            this.loadedChildCards.indexOf('card2') >= 0 && this.loadedChildCards.indexOf('card3') >= 0) {
            if (this.isDesktopSize) {
                this.cssStyleClass = 'nds-grid nds-wrap nds-p-around_xx-large nds-align_absolute-center card';
            } else {
                this.cssStyleClass = 'nds-grid nds-wrap nds-p-around_small nds-align_absolute-center card-mobile';
            }
            this.isLoaded = true;
            this.showHeader = true;
        }
    }

    /*
    Method Name : invokeCreateBasket
    Description : invokes the create Basket operation to get the pricing for respective card
    Paramerter  : NA
    */
    invokeCreateBasket(energyType,month) {
        this.month = month;
        return new Promise((resolve, reject) => {
            this.findAndReplace(this._jsonResult, "code", "ATT_CONTRACT_DURATION", "userValues", month);
            if(this._commodity == 'GAS') {
                this.findAndReplace(this._jsonResult, "code", "ATT_GREENENERGY_GAS", "userValues", energyType); //green
            } else {
                this.findAndReplace(this._jsonResult, "code", "ATT_GREEN_ENERGY_ELEC", "userValues", energyType);
            }
            
            // Instantiate the input object for addToBasket to specify parameters
            let input = this._digitalCommerce.createAddToCartInput();
            input.catalogCode = this.catalogCode; // use your Catalog Code
            input.basketAction = "AddAfterConfig";
            input.offerConfiguration = this._jsonResult; // adding an offer that has been configured (change in its json structure from getOfferDetails) to basket
            input.customFields = ["OrderItem", "vlocity_cmt__UsageUnitPrice__c"];

            // Invoke Basket API via method addToBasket()
            this._digitalCommerce
                .addToCart(input)
                .then(response => {
                    resolve(response);
                    console.log('basket response====', response);
                    this.isLoaded = true;
                    let innerNode = response.cartItems[0].lineItems;
                    let charges = {};
                    for (let key in innerNode) {
                        if (key != 'remove') {
                            if (innerNode[key].name == 'Standard') {
                                this.standardCharge = innerNode[key].customFields.vlocity_cmt__UsageUnitPrice__c.value;
                                charges.standard =  this.standardCharge;
                                this.chargeHeader.standard = true;
                            } else if (innerNode[key].name == 'STANDING CHARGE') {
                                this.standingCharge = innerNode[key].customFields.vlocity_cmt__UsageUnitPrice__c.value;
                                charges.standingCharge = this.standingCharge;
                                this.chargeHeader.standingCharge = true;
                                //this.standingCharge = parseFloat((standCharge / 100).toFixed(2));
                            } else if (innerNode[key].name == 'Day') {
                                this.dayCharge = innerNode[key].customFields.vlocity_cmt__UsageUnitPrice__c.value;
                                charges.dayCharge = this.dayCharge;
                                this.chargeHeader.dayCharge = true;
                            } else if (innerNode[key].name == 'Night') {
                                this.nightCharge = innerNode[key].customFields.vlocity_cmt__UsageUnitPrice__c.value;
                                charges.nightCharge = this.nightCharge;
                                this.chargeHeader.nightCharge = true;
                            } else if (innerNode[key].name == 'Evening/Weekend') {
                                this.eveningWeekendCharge = innerNode[key].customFields.vlocity_cmt__UsageUnitPrice__c.value;
                                charges.eveningWeekendCharge = this.eveningWeekendCharge;
                                this.chargeHeader.eveningWeekendCharge = true;
                            } else if (innerNode[key].name == 'Off-Peak') {
                                this.offPeakCharge = innerNode[key].customFields.vlocity_cmt__UsageUnitPrice__c.value;
                                charges.offPeakCharge = this.offPeakCharge;
                                this.chargeHeader.offPeakCharge = true;
                            } else if (innerNode[key].name == 'Day 1') {
                                this.day1Charge = innerNode[key].customFields.vlocity_cmt__UsageUnitPrice__c.value;
                                charges.day1Charge = this.day1Charge;
                                this.chargeHeader.day1Charge = true;
                            } else if (innerNode[key].name == 'Day 2') {
                                this.day2Charge = innerNode[key].customFields.vlocity_cmt__UsageUnitPrice__c.value;
                                charges.day2Charge = this.day2Charge;
                                this.chargeHeader.day2Charge = true;
                            } else if (innerNode[key].name == 'Day 3') {
                                this.day3Charge = innerNode[key].customFields.vlocity_cmt__UsageUnitPrice__c.value;
                                charges.day3Charge = this.day3Charge;
                                this.chargeHeader.day3Charge = true;
                            } else if (innerNode[key].name == 'Day 4') {
                                this.day4Charge = innerNode[key].customFields.vlocity_cmt__UsageUnitPrice__c.value;
                                charges.day4Charge = this.day4Charge;
                                this.chargeHeader.day4Charge = true;
                            } else if (innerNode[key].name == 'Day 5') {
                                this.day5Charge = innerNode[key].customFields.vlocity_cmt__UsageUnitPrice__c.value;
                                charges.day5Charge = this.day5Charge;
                                this.chargeHeader.day5Charge = true;
                            }                           
                        }
                    }
                    this.year = parseInt(this.month) / 12;
                    if (this.commodity == 'GAS') {
                        if (this.standardCharge && this.standingCharge) {
                            this.annualCost = (((parseInt(this.usage) * this.standardCharge) + (this.standingCharge * 365)).toFixed(2));
                            this.monthlyCost = ((this.annualCost / 12).toFixed(2));
                            this.contractCost = ((this.annualCost * (this.month / 12)).toFixed(2));
                        } else {
                            this.isFailed = true;
                            this.annualCost = 0.00;
                            this.monthlyCost = 0.00;
                            this.contractCost = 0.00;
                        }
                    } else {
                        if (this.rateStructure) {
                            if (this.rateStructure.rateStructure == 'Standard' && this.standardCharge) {
                                this.annualCost = (((parseInt(this.usage) * this.standardCharge) + (this.standingCharge * 365)).toFixed(2));
                                this.monthlyCost = ((this.annualCost / 12).toFixed(2));
                                this.contractCost = ((this.annualCost * (this.month / 12)).toFixed(2));
                            } else if (this.rateStructure.rateStructure == 'DayNight' && this.dayCharge && this.nightCharge) {
                                let price = 0;
                                if (this.dayCharge) {
                                    price += (this.dayCharge * parseFloat(this.rateStructure.dayPercentage));
                                }
                                if (this.nightCharge) {
                                    price += (this.nightCharge * parseFloat(this.rateStructure.nightPercentage));
                                }
                                if (this.day1Charge) {
                                    price += (this.day1Charge * parseFloat(this.rateStructure.day1Percentage))
                                }
                                if (this.day2Charge) {
                                    price += (this.day2Charge * parseFloat(this.rateStructure.day2Percentage))
                                }
                                if (this.day3Charge) {
                                    price += (this.day3Charge * parseFloat(this.rateStructure.day3Percentage))
                                }
                                if (this.day4Charge) {
                                    price += (this.day4Charge * parseFloat(this.rateStructure.day4Percentage))
                                }
                                if (this.day5Charge) {
                                    price += (this.day5Charge * parseFloat(this.rateStructure.day5Percentage))
                                }
                                this.annualCost = (((parseInt(this.usage) * (price / 100)) + (this.standingCharge * 365)).toFixed(2));
                                this.monthlyCost = ((this.annualCost / 12).toFixed(2));
                                this.contractCost = ((this.annualCost * (month / 12)).toFixed(2));
                            } else if (this.rateStructure.rateStructure == 'EveningAndWeekend' && this.dayCharge && this.eveningWeekendCharge) {
                                let price = (this.dayCharge * parseFloat(this.rateStructure.dayPercentage)) + (this.eveningWeekendCharge * parseFloat(this.rateStructure.ewePercentage));
                                this.annualCost = (((parseInt(this.usage) * (price / 100)) + (this.standingCharge * 365)).toFixed(2));
                                this.monthlyCost = ((this.annualCost / 12).toFixed(2));
                                this.contractCost = ((this.annualCost * (this.month / 12)).toFixed(2));
                            } else if (this.rateStructure.rateStructure == 'EveningWeekendandNight' && this.dayCharge && this.nightCharge && this.eveningWeekendCharge) {
                                let price = (this.dayCharge * parseFloat(this.rateStructure.dayPercentage)) +
                                    (this.nightCharge * parseFloat(this.rateStructure.nightPercentage)) +
                                    (this.eveningWeekendCharge * parseFloat(this.rateStructure.ewePercentage));
                                this.annualCost = (((parseInt(this.usage) * (price / 100)) + (this.standingCharge * 365)).toFixed(2));
                                this.monthlyCost = ((this.annualCost / 12).toFixed(2));
                                this.contractCost = ((this.annualCost * (this.month / 12)).toFixed(2));
                            } else if (this.rateStructure.rateStructure == 'OffPeak' && this.offPeakCharge) {
                                let price = (this.offPeakCharge * parseFloat(this.rateStructure.offPeakPercentage));
                                this.annualCost = (((parseInt(this.usage) * (price / 100)) + (this.standingCharge * 365)).toFixed(2));
                                this.monthlyCost = ((this.annualCost / 12).toFixed(2));
                                this.contractCost = ((this.annualCost * (this.month / 12)).toFixed(2));
                            } else {
                                this.isFailed = true;
                                this.annualCost = 0.00;
                                this.monthlyCost = 0.00;
                                this.contractCost = 0.00;
                                return;
                            }

                        } else {
                            this.handleOnFailure();                            
                        }                       
                    }
                    if(energyType == 'STANDARD')
                    {
                        charges.productName = month/12 + ' year' + ' fixed';
                    } else {
                        charges.productName = month/12 + ' year ' + energyType + ' fixed';
                    }
                    
                    charges.annualCost = this.annualCost ;
                    charges.monthlyCost = this.monthlyCost;
                    charges.contractCost = this.contractCost;
                    this.prices.push(charges);
                    
                    if(this.prices.length === 6)
                    {
                        this.isPriceLoaded = true;
                        this.isLoaded = true;
                        this.isFailed = false
                        this.prices.sort(this.dynamicSort("productName"));
                        if(this.commodity == 'GAS') {
                            this.omniApplyCallResp({"gasPrices":this.prices});
                        } else {
                            this.omniApplyCallResp({"elecPrices":this.prices});
                        }

                        if(this.multiCommodity && this.commodity == "GAS")
                        {
                            this.isEmailQuote = true;
                        } else if(!this.multiCommodity) {
                            this.isEmailQuote = true;
                        }
                                          
                    }                  
                })
                .catch(error => {
                    console.log("addToBasket rest call failed : " + error + JSON.stringify(error));
                    this.handleOnFailure();
                    //this.sendEvtToParentOnFailure();
                    this.isLoaded = true;
                    this.isFailed = true;
                });
        });
    }
    handleRowSelection(event){
        const selectedRows = event.target.value;
        let selectedPlan = this.prices[selectedRows];
        console.log("selectedPlan------", selectedPlan);
        this.prices.forEach(row =>{
            if(row.productName === selectedPlan.productName){
                row.rowSelected = 'row-color';
            } else {
                row.rowSelected = '';
            }
        });
        if(this.commodity == 'GAS') {
            this.omniApplyCallResp({"gasSelectedPrices":selectedPlan});
        } else {
            this.omniApplyCallResp({"elecSelectedPrices":selectedPlan});
        }  
    }
    handleStDateCahngeEvent(event){
        this.startDt = event.target.value;
        this._jsonResult = this.injectUserDataIntoGetOfferDetails(this._jsonResult);
        this.isPriceLoaded = false;
        this.calculatePrices();
        this.isLoaded = false;
    }
    sendEmail(event){
        console.log("Email should be sent====");
    }
    dynamicSort(property) {
        var sortOrder = 1;
        if(property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a,b) {
            /* next line works with strings and numbers, 
             * and you may want to customize it to your needs
             */
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    }
}